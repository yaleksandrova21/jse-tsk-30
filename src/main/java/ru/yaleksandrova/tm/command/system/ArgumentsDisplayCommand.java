package ru.yaleksandrova.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.yaleksandrova.tm.command.AbstractCommand;
import java.util.Collection;

public final class ArgumentsDisplayCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "arguments";
    }

    @NotNull
    @Override
    public String arg() {
        return "-arg";
    }

    @NotNull
    @Override
    public String description() {
        return "Display list arguments";
    }

    @Override
    public void execute() {
        System.out.println("[ARGUMENTS]");
        @NotNull final Collection<AbstractCommand> arguments = serviceLocator.getCommandService().getArguments();
        for (@NotNull final AbstractCommand argument : arguments)
            System.out.println(argument.arg());

    }

}
