package ru.yaleksandrova.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.yaleksandrova.tm.command.AbstractUserCommand;
import ru.yaleksandrova.tm.model.User;
import ru.yaleksandrova.tm.util.ApplicationUtil;

public class UserUpdateByLoginCommand extends AbstractUserCommand {

    @NotNull
    @Override
    public String name() {
        return "user-update-by-login";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Update user profile";
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE PROFILE]");
        System.out.println("ENTER USER LOGIN: ");
        @NotNull final String login = ApplicationUtil.nextLine();
        final User user = serviceLocator.getUserService().findByLogin(login);
        System.out.println("ENTER FIRST NAME:");
        @NotNull final String firstName = ApplicationUtil.nextLine();
        System.out.println("ENTER LAST NAME:");
        @NotNull final String lastName = ApplicationUtil.nextLine();
        System.out.println("ENTER MIDDLE NAME:");
        @NotNull final String middleName = ApplicationUtil.nextLine();
        System.out.println("ENTER EMAIL:");
        @NotNull final String email = ApplicationUtil.nextLine();
        serviceLocator.getUserService().updateUserByLogin(login, firstName, lastName, middleName, email);
        System.out.println("User profile updated!");
    }

}
