package ru.yaleksandrova.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.yaleksandrova.tm.command.AbstractTaskCommand;
import ru.yaleksandrova.tm.enumerated.Role;
import ru.yaleksandrova.tm.enumerated.Status;
import ru.yaleksandrova.tm.exception.entity.TaskNotFoundException;
import ru.yaleksandrova.tm.model.Task;
import ru.yaleksandrova.tm.util.ApplicationUtil;

public final class TaskChangeStatusByNameCommand extends AbstractTaskCommand {

    @NotNull
    @Override
    public String name() {
        return "task-change-status-by-name";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Change task status by name";
    }

    @Override
    public void execute() {
        @NotNull final String userId = serviceLocator.getAuthService().getCurrentUserId();
        System.out.println("[ENTER NAME]");
        @NotNull final String name = ApplicationUtil.nextLine();
        System.out.println("[ENTER STATUS]");
        @NotNull final String statusValue = ApplicationUtil.nextLine();
        final Status status = Status.valueOf(statusValue);
        final Task task = serviceLocator.getTaskService().changeStatusByName(userId, name, status);
        if (task == null) {
            throw new TaskNotFoundException();
        }
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[] {Role.USER};
    }

}
