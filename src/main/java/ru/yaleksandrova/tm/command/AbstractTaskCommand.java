package ru.yaleksandrova.tm.command;

import org.jetbrains.annotations.NotNull;
import ru.yaleksandrova.tm.exception.empty.EmptyNameException;
import ru.yaleksandrova.tm.model.Task;

public abstract class AbstractTaskCommand extends AbstractCommand{

    protected void showTask(@NotNull final Task task) {
        System.out.println("Id: " + task.getId());
        System.out.println("Name: " + task.getName());
        System.out.println("Description: " + task.getDescription());
        System.out.println("Status: " + task.getStatus().getDisplayName());
        System.out.println("Project Id: " + task.getProjectId());
        System.out.println("Created: " + task.getCreated());
        System.out.println("Start Date: " + task.getStartDate());
        System.out.println("Finish Date: " + task.getFinishDate());
    }

    protected Task add(final String name, final String description) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) return null;
        return new Task(name, description);
    }

}
