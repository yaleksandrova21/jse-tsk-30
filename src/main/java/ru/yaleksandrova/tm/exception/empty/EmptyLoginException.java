package ru.yaleksandrova.tm.exception.empty;

import ru.yaleksandrova.tm.exception.AbstractException;

public class EmptyLoginException extends AbstractException {

    public EmptyLoginException() {
        super("Error! Login cannot be empty");
    }

}
