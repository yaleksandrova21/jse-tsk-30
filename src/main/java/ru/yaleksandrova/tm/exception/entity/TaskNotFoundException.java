package ru.yaleksandrova.tm.exception.entity;

import ru.yaleksandrova.tm.exception.AbstractException;

public class TaskNotFoundException extends AbstractException {

    public TaskNotFoundException() {
        super("Error! Task not found");
    }

}
