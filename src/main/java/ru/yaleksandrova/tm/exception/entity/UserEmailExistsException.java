package ru.yaleksandrova.tm.exception.entity;

import ru.yaleksandrova.tm.exception.AbstractException;

public class UserEmailExistsException extends AbstractException {

    public UserEmailExistsException(String email) {
        super("Error! User with this email '" + email + "' already exist");
    }

}
